'use strict'

const path = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const BabiliPlugin = require('babili-webpack-plugin')
const isProduction = process.env.NODE_ENV === 'production'

const config = {
  entry: {
    app: './client/src/index.js'
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'client', 'dist')
  },
  resolve: {
    alias: {
      '@normalize': path.resolve('server/src/lib/normalize'),
      '@store': path.resolve('server/src/lib/store')
    }
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          use: [
            { loader: 'css-loader', options: { importLoaders: 1 } },
            { loader: 'postcss-loader' }
          ]
        })
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env', 'flow'],
            babelrc: false
          }
        }
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin('[name].css')
  ]
}

if (isProduction) {
  config.plugins.push(new BabiliPlugin())
} else {
  config.devtool = 'inline-source-map'
  config.devServer = {
    contentBase: path.resolve(__dirname, 'client'),
    publicPath: '/dist/',
    compress: true,
    overlay: true,
    watchOptions: {
      ignored: /node_modules/
    },
    proxy: {
      '/import': 'http://localhost:3000',
      '/search': 'http://localhost:3000'
    }
  }
}

module.exports = config
