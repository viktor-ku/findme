// @flow

import 'normalize.css'
import './styles/index.css'

import Papa from 'papaparse'
import Nanobar from 'nanobar'
import alertify from 'alertify.js'
import store from '@store'
import normalize from '@normalize'
import uploadField from './components/uploadField'
import uploadButton from './components/uploadButton'
import view from './components/view'
import searchField from './components/searchField'
import card from './components/card'
import name from './components/name'
import { upload, search, cards } from './lib/el'

window.addEventListener('hashchange', e => {
  const name: string = location.hash.slice(1)

  if (!name) {
    return
  }

  searchField.dispatch(name, true)
})

uploadField.addEventListener()

view.subscribe(name => {
  uploadField.removeEventListener()
  searchField.addEventListener()
  uploadButton.addEventListener()
  upload.classList.add('hidden')
  search.classList.remove('hidden')
})

searchField.subscribe((value: string) => {
  while (cards.firstChild) {
    cards.removeChild(cards.firstChild)
  }

  const normValue: string = normalize(value)
  const results: Array<Object> = store.get(normValue)
  const first: Object = results[0]

  if (first && normalize(first.name) === normValue) {
    results.shift()
    cards.appendChild(card.render(first))
  }

  for (let n = 0, len = results.length; n < len; n++) {
    cards.appendChild(name.render(results[n], normValue.length))
  }
})

uploadField.subscribe(file => {
  const bar = new Nanobar()
  uploadField.getLabel().classList.add('button--disabled')
  uploadField.getInput().disabled = true
  uploadField.removeEventListener()
  bar.go(10)

  Papa.parse(file, {
    complete (result) {
      let inserted = 0

      for (let n = 0, len = result.data.length; n < len; n++) {
        const line: Array<string> = result.data[n]
        if (line.length !== 5) {
          continue
        }
        const id = line[0]
        const name = line[1]
        const age = line[2]
        const address = line[3]
        const team = line[4]
        store.set(normalize(name), { id, name, age, address, team })
        inserted++
      }

      alertify.success(`${inserted} items imported`)
      bar.go(100)
      view.go('search')
    }
  })
})

uploadButton.subscribe((e: Event) => {
  const file: File = uploadField.getFile()
  const request = new XMLHttpRequest()
  const data = new FormData()
  const blob = new Blob([file], { type: file.type })
  const bar = new Nanobar()
  let width = 5
  data.append('file', blob)
  uploadButton.getButton().disabled = true
  bar.go(width)

  const timer = setInterval(() => {
    if (width > 90) {
      clearInterval(timer)
    }
    bar.go(width += 0.2)
  }, 50)

  request.onload = (e: ProgressEvent) => {
    const { responseText, status, statusText } = e.target

    bar.go(100)
    clearInterval(timer)

    if (status !== 200) {
      console.error(status, statusText, responseText)
      alertify.error('Something wrong just happened (see console for details)')
      uploadButton.getButton().disabled = false
      return
    }

    alertify.success('You can access your data from another service now')
    uploadButton.removeEventListener()
    uploadButton.getButton().classList.add('hidden')
  }

  request.open('POST', '/import', true)
  request.send(data)
})
