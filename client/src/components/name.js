// @flow

interface State {
  id: string,
  name: string
}

function render (state: State, inputLen: number): Element {
  const a: HTMLAnchorElement = document.createElement('a')
  const mark: Element = document.createElement('mark')
  const name = state.name

  mark.appendChild(document.createTextNode(name.slice(0, inputLen)))

  a.href = '#' + name
  a.classList.add('name')
  a.id = 'result-' + state.id
  a.appendChild(mark)
  a.appendChild(document.createTextNode(name.slice(inputLen)))

  return a
}

export default {
  render
}
