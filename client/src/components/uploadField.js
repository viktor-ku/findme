// @flow

import alertify from 'alertify.js'
import { uploadField, uploadLabel } from '../lib/el'

const state: {
  file?: File,
  listener: Function
} = {
  listener: function none () {}
}

function subscribe (listener: Function) {
  state.listener = listener
}

const addEventListener = () => uploadField.addEventListener('change', handler)
const removeEventListener = () => uploadField.removeEventListener('change', handler)
const getFile = (): File => state.file
const getInput = (): HTMLInputElement => uploadField
const getLabel = (): HTMLLabelElement => uploadLabel

function handler () {
  const file: File = this.files[0]

  if (!file) {
    alertify.error('Select file please')
    return
  }

  if (!file.size) {
    alertify.error('File should not be empty')
    return
  }

  if (file.type !== 'text/csv') {
    alertify.error('CSV file is expected')
    return
  }

  state.file = file
  state.listener(file)
}

export default {
  getFile,
  getInput,
  getLabel,
  subscribe,
  addEventListener,
  removeEventListener
}
