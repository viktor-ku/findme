// @flow

const state: {
  name: string,
  listener: Function
} = {
  name: 'upload',
  listener: function none () {}
}

function go (name: string) {
  state.listener(name)
  state.name = name
}

function subscribe (listener: Function) {
  state.listener = listener
}

export default {
  go,
  subscribe
}
