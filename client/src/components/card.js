// @flow

interface State {
  id: string,
  name: string,
  age: string,
  address: string,
  team: string
}

function render (state: State): Element {
  const div: Element = document.createElement('div')
  const pName: Element = document.createElement('p')
  const pTeam: Element = document.createElement('p')
  const pAddress: Element = document.createElement('p')
  const pAge: Element = document.createElement('p')
  const circle: Element = document.createElement('div')
  const color = state.team.toLowerCase()

  circle.classList.add('circle')
  circle.style.background = color

  if (color === 'white') {
    circle.style['border'] = '1px solid #eee'
  }

  div.id = 'result-' + state.id
  div.classList.add('card')

  pName.appendChild(document.createTextNode(state.name))
  pName.appendChild(circle)
  pName.classList.add('lead')

  pAge.appendChild(document.createTextNode(state.age + ' years old'))

  pTeam.appendChild(document.createTextNode('Team colour is ' + color))

  pAddress.appendChild(document.createTextNode(state.address))

  div.appendChild(pName)
  div.appendChild(pAge)
  div.appendChild(pTeam)
  div.appendChild(pAddress)

  return div
}

export default {
  render
}
