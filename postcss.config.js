const autoprefixer = require('autoprefixer')
const cssnano = require('cssnano')

module.exports = ({ file, options, env }) => {
  const config = {
    plugins: [
      autoprefixer({
        browsers: 'last 2 versions'
      })
    ]
  }

  if (env === 'production') {
    config.plugins.push(cssnano())
  }

  return config
}
