// @flow
'use strict'

import Node from './Node'

type Value = Object
type Char = string

class Store {
  rootSocket: Object
  table: Object

  constructor () {
    this.rootSocket = {}
    this.table = {}
  }

  toObject (): Object {
    const obj = {}
    const keys = Object.keys(this.table)

    for (let n = 0, len = keys.length; n < len; n++) {
      const key = keys[n]
      const value = this.table[key].value
      obj[key] = value
    }

    return obj
  }

  toJSON (): string {
    return JSON.stringify(this.toObject())
  }

  closest (query: string): Node | null {
    let node = new Node({ socket: this.rootSocket })

    for (let n = 0, len = query.length; n < len; n++) {
      const char = query[n]

      if (!node.socket[char]) {
        return null
      }

      node = node.socket[char]
    }

    return node
  }

  get (query: string, count: number = 20): Array<Value> {
    if (!query) {
      return []
    }

    const points: Array<Node> = []
    const result: Array<Value> = []
    const closestNode: Node | null = this.closest(query)

    if (!closestNode) {
      return []
    }

    let node: Node = closestNode

    while (true) {
      if (result.length >= 20) {
        break
      }

      const sockets: Array<Char> = Object.keys(node.socket)
      const socketCount: number = sockets.length
      let char: Char = sockets[0]

      if (node.name) {
        result.push(node.value)
      }

      if (socketCount >= 2) {
        for (let n = 1, len = sockets.length; n < len; n++) {
          points.push(node.socket[sockets[n]])
        }
      } else if (socketCount === 0) {
        const next: Node = points.pop()
        if (!next) {
          break
        }
        node = next
        continue
      }

      node = node.socket[char]
    }

    return result
  }

  set (key: string, value: Value): boolean {
    if (!key) {
      return false
    }

    let node = new Node({ socket: this.rootSocket })

    for (let n = 0, len = key.length; n < len; n++) {
      const char: Char = key[n]

      if (!node.socket[char]) {
        node.socket[char] = new Node()
      }

      node = node.socket[char]
    }

    node.name = key
    node.value = value
    this.table[key] = node
    return true
  }

  reset (): boolean {
    // possible memory leak
    // maybe need to explicitly delete all nodes
    // but maybe not in this case as more than one user is not expected
    this.rootSocket = {}
    this.table = {}
    return true
  }
}

export default Store
