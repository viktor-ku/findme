// @flow
'use strict'

import getFile from '../lib/getFile'

const files = new Map()

files.set('index.html', getFile('index.html'))
files.set('app.js', getFile('dist/app.js'))
files.set('app.css', getFile('dist/app.css'))

export default files
