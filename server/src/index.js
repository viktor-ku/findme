// @flow
'use strict'

import Koa from 'koa'
import koaBody from 'koa-body'
import os from 'os'
import importRoute from './routes/import'
import cssRoute from './routes/app-css'
import jsRoute from './routes/app-js'
import homeRoute from './routes/home'
import searchRoute from './routes/search'

const app = new Koa()
const port = process.env.NODE_PORT || 3000

app
  .use(homeRoute)
  .use(jsRoute)
  .use(cssRoute)
  .use(koaBody({
    multipart: true,
    formidable: {
      uploadDir: os.tmpdir()
    }
  }))
  .use(importRoute)
  .use(searchRoute)

app.listen(port, () => {
  console.log('Server listening on port %s', port)
})
