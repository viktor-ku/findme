// @flow
'use strict'

import files from '../lib/files'

function jsRoute (x: Object, next: Function) {
  if (x.method !== 'GET' || x.url !== '/dist/app.js') {
    return next()
  }

  x.type = 'javascript'
  x.body = files.get('app.js') || Buffer.from('')
  return x.body
}

export default jsRoute
