// @flow
'use strict'

import store from '../lib/store'
import normalize from '../lib/normalize'

function searchRoute (x: Object, next: Function) {
  if (x.method !== 'POST' || x.url !== '/search') {
    return next()
  }

  const { query } = x.request.body

  if (!query) {
    return x.throw(400, 'query is expected', { query })
  }

  x.body = {
    results: store.get(normalize(query))
  }
}

export default searchRoute
