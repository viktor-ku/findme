// @flow
'use strict'

import files from '../lib/files'

function cssRoute (x: Object, next: Function): Buffer {
  if (x.method !== 'GET' || x.url !== '/dist/app.css') {
    return next()
  }

  x.type = 'css'
  x.body = files.get('app.css') || Buffer.from('')
  return x.body
}

export default cssRoute
