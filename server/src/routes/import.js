// @flow
'use strict'

import csvParser from 'fast-csv'
import store from '../lib/store'
import normalize from '../lib/normalize'

async function importRoute (x: Object, next: Function) {
  if (x.method !== 'POST' || x.url !== '/import') {
    return next()
  }

  if (!x.is('multipart/form-data')) {
    return x.throw(400)
  }

  const body = x.request.body

  if (!body || !body.files || !body.files.file) {
    return x.throw(400)
  }

  const file: {
    size: number,
    path: string,
    name: string,
    type: string
  } = body.files.file

  if (!file.size || file.type !== 'text/csv') {
    return x.throw(400)
  }

  store.reset()
  await parse(file)

  x.body = {
    ok: true
  }

  return x.body
}

const parse = file => new Promise((resolve, reject) => {
  csvParser
    .fromPath(file.path)
    .transform(line => ({
      id: line[0],
      name: line[1],
      age: line[2],
      address: line[3],
      team: line[4]
    }))
    .validate(x => x && x.id && x.name && x.age && x.address && x.team)
    .on('data', x => store.set(normalize(x.name), x))
    .on('end', resolve)
})

export default importRoute
