# FindMe App

> Find anyone by name from the imported list

## Features

  - Search by names
  - Autocompletion displays up to 20 possible results
  - Displays real-time progress upon importing large files
  - `uploadField`, `uploadButton`, `searchField`, `result-X` guaranteed elements
  - `GET /` for the `index.html`
  - `POST /import` for file upload
  - `POST /search` for searching

## Searching

#### payload

```js
const request = {
  query: "Johns"
}
```

#### Result

```js
const response = {
  results: [
    {
      id: String,
      name: String,
      age: String,
      address: String,
      team: String
    }
  ]
}
```
