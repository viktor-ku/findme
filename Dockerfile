FROM node:8-alpine

EXPOSE 3000

ENV NODE_PORT 3000
ENV NODE_ENV production
ENV WORKDIR /home/node/app
WORKDIR $WORKDIR

COPY client/src client/src
COPY client/index.html client/index.html
COPY server/src server/src
COPY package.json package.json
COPY package-lock.json package-lock.json
COPY .babelrc .babelrc
COPY webpack.config.js webpack.config.js

RUN npm install

RUN npm run build && \
    rm -rf \
      client/src \
      server/src \
      .babelrc \
      webpack.config.js \
      package-lock.json \
      && \
    chown -R node:node /home/node/app

USER node

CMD npm start
