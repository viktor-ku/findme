// @flow
'use strict'

import t from 'tap'
import request from 'request-promise'

t.test('POST /search', async t => {
  const response = await request({
    method: 'POST',
    uri: 'http://localhost:3000/search',
    body: {
      query: '     michael jackson    '
    },
    json: true,
    simple: false
  })

  t.ok(response)
  t.ok(response.results)
  t.equal(response.results.length, 1)
  t.equal(response.results[0].name, 'Michael Jackson')
  t.equal(response.results[0].age, '55')
  t.equal(response.results[0].id, '41116')

  t.end()
})
